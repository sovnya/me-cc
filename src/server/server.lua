local server_api = require("api").server
local surface_api = require("api").surface
local log = require("api").log

-- Open rednet connection
server_api.modem.open()

-- Initialize the monitor
local screen = peripheral.find("monitor") or error("No monitor attached", 0)
local x, y = screen.getSize()

local tab = surface_api.create(x, 1, " ", colors.gray, colors.white)
local main = surface_api.create(x, y, " ", colors.blue, colors.white)

tab:drawText(1, 1, "ME-CC")
main:drawText(1, 1, "A simple, yet powerful ME system made in Lua with ComputerCraft.")

tab:render(screen, 1, 1)
main:render(screen, 1, 2)

while true do
    local chests = server_api.modem.recieve_json()
    log.info("Recieved JSON, rendering screen")
    main:drawText(1, 3, "- Current items:")

    for i, chest in pairs(chests) do
        local name = chest.name
        local count = chest.count
        local slots = chest.slots
        local _type = chest.type

        local format = string.format("- %s: %d (%d slots, %s)", name, count, slots, _type)
        main:drawText(2, 3 + i, format)
    end

    tab:render(screen, 1, 1)
    main:render(screen, 1, 2)
end