local client_api = require("api").client
local log = require("api").log

local chests = client_api.chests
local modem = client_api.modem

modem.open()

while true do
    local collectedChests = {}
    local tChests = chests.getChests()

    for _, chest in pairs(tChests) do
        local inv = chests.getInventoryByChestName(chest)
        local slots = #inv
        local chest_type = (chest:find("barrel") and "barrel") or (chest:find("furnace") and "furnace") or "chest"
        if slots > 0 then
            if chests.checkIfChestIsFilledWithSingleItem(chest) then
                local count = 0

                for i, _ in pairs(inv) do
                    local detail = chests.getItemDetailByChestName(chest, i)
                    count = count + detail.count
                end

                local data = chests.getItemDetailByChestName(chest, 1)

                collectedChests[#collectedChests + 1] = {
                    name = data.displayName,
                    count = count,
                    type = chest_type,
                    slots = slots
                }
            else
                for i=1, slots, 1 do
                    local data = chests.getItemDetailByChestName(chest, i)
                    local name = data.displayName
                    local count = data.count

                    collectedChests[#collectedChests + 1] = {
                        name = name,
                        count = count,
                        type = chest_type,
                        slots = slots
                    }
                end
            end
        end
    end

    modem.send_json(collectedChests)
    log.info("Sent chests to server")
    os.sleep(1)
end