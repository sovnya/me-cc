local modem_api = {}

--------------------------------------
-- MODEM API
--------------------------------------

--- Open the transmission port
-- Open's a rednet connection which is used by the client to send data to the server
-- @param modem The modem's side
function modem_api.open()
    modem_api.modem = peripheral.find("modem")
    if modem_api.modem ~= nil then
        modem_api.modem.open(13372)
    end
end

--- Recieve JSON data
-- Recieve's JSON data sent by a client
-- @return Lua object containing the JSON data sent by the client
function modem_api.recieve_json()
    local _, _, channel, _, msg, _ = os.pullEvent("modem_message")
    if channel == 13372 then
        local chests = textutils.unserialiseJSON(msg)
        return chests
    end
    return nil
end

--- Close the transmission port
-- Closes the transmission port made by `modem_api.open`
-- @param modem The modem's side
function modem_api.close()
    if modem_api.modem ~= nil and modem_api.modem.isOpen(13372) then
        modem_api.modem.close(13372)
        modem_api.modem = nil
    end
end

return {
    modem = modem_api
}