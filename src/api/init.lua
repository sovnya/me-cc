-- API for the client and the server.
-- This includes getting item information from multiple types of chests.

local client_api  = require("api.client_api")
local server_api  = require("api.server_api")
local log_api     = require("api.log_api")
local surface_api = require("api.surface_api")

return {
    server = server_api,
    client = client_api,
    log = log_api,
    surface = surface_api
}