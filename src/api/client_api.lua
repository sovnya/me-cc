local chests_api = {}
local modem_api = {}

--------------------------------------
-- CHESTS API
--------------------------------------

--- Get chests connected to computer
-- Gets chests (or barrels, furnaces, etc) from a computer
-- @return a table with all the chests/barrels
function chests_api.getChests()
    local chests = peripheral.getNames()
    local actual_chests = {}

    for i = 1, #chests do
        if chests[i]:find("chest") or chests[i]:find("barrel") or chests[i]:find("furnace") then
            actual_chests[#actual_chests + 1] = chests[i]
        end
    end

    return actual_chests
end

--- Check if chest is filled with single item
-- Checks if a chest is filled with a single item.
-- @return a table with all the chests/barrels
function chests_api.checkIfChestIsFilledWithSingleItem(chest)
    local inventory = peripheral.call(chest, "list")
    local size = peripheral.call(chest, "size")
    local count = 0

    for i, slot in pairs(inventory) do
        if slot.count == 64 then
            count = count + 1
        end
    end

    if count == size then return true end

    return false
end

--- Get the inventory of a chest by name
-- Gets the intentory of a chest (or barrel) by name
-- @param name a string value (Ex: minecraft:barrel_0)
-- @return The inventory of the chest
function chests_api.getInventoryByChestName(name)
    return peripheral.call(name, "list")
end

--- Get the item information of a chest by its slot
-- Gets the item information of a chest by its slot
-- @param name a string value (Ex: minecraft:barrel_0)
-- @param slot an interger value (Ex: 1)
-- @return The inventory of the chest
function chests_api.getItemDetailByChestName(name, slot)
    local detail = peripheral.call(name, "getItemDetail", slot)
    return detail
end

--------------------------------------
-- MODEM API
--------------------------------------

--- Open a rednet connection
-- Open's a rednet connection which is used by the client to send data to the server
-- @param modem The modem's side
function modem_api.open()
    modem_api.modem = peripheral.find("modem") or nil
end

--- Send JSON data
-- Sends JSON data to all computers
-- @param data_obj The JSON data as a lua object
function modem_api.send_json(data_obj)
    if modem_api.modem ~= nil then
        modem_api.modem.transmit(13372, 0, textutils.serialiseJSON(data_obj))
    end
end

--- Close a rednet connection
-- Closes the rednet connection made by `modem_api.open`
-- @param modem The modem's side
function modem_api.close()
    if modem_api.modem ~= nil and modem_api.modem.isOpen(13372) then
        modem_api.modem.close(13372)
    end
end

return {
    chests = chests_api,
    modem = modem_api
}